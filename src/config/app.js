//FireBase
exports.firebaseConfig = {
    apiKey: "AIzaSyCCpIwAIqLVFsgzXUMMubBcd2Gb1RaD5fo",
    authDomain: "kreed-of-sports.firebaseapp.com",
    databaseURL: "https://kreed-of-sports.firebaseio.com",
    projectId: "kreed-of-sports",
    storageBucket: "kreed-of-sports.appspot.com",
    messagingSenderId: "142735995489"
  };


//App setup
exports.adminConfig={
  "appName": "KreedOfSports",
  "slogan":"Powered by",
  "design":{
    "sidebarBg":"sidebar-5.jpg", //sidebar-1, sidebar-2, sidebar-3
    "dataActiveColor":"rose", //"purple | blue | green | orange | red | rose"
    "dataBackgroundColor":"black", // "white | black"
  },
  "showItemIDs":false,
  "allowedUsers":["karnatakaswimming@gmail.com"], //If null, allow all users, else it should be array of allowd users
  "allowGoogleAuth":false, //Allowed users must contain list of allowed users in order to use google auth
  "fieldBoxName": "Fields",
  "maxNumberOfTableHeaders":5,
  "prefixForJoin":["-event"],
  "showSearchInTables":true,
  "methodOfInsertingNewObjects":"push", //timestamp (key+time) | push - use firebase keys
  "urlSeparator":"+",
  "urlSeparatorFirestoreSubArray":"~",
  "googleMapsAPIKey":"AIzaSyCHIlM68tfKslBZ31_r0PrJfho4qBAFaLE",

  "fieldsTypes":{
    "photo":["photo","image"],
    "dateTime":["end","start"],
    "map":["map","latlng","location"],
    "textarea":["description"],
    "html":["content"],
    "radio":["radio","radiotf","featured"],
    "checkbox":["checkbox"],
    "dropdowns":["status","dropdowns"],
    "file":["video"],
    "rgbaColor":['rgba'],
    "hexColor":['color'],
    "relation":['type','creator'],
    "iconmd":['icon'],
    "iconfa":['iconfa'],
    "iconti":['iconti'],
  },
  "optionsForDateTime":[
    {"key":"end", "dateFormat":"YYYY-MM-DD" ,"timeFormat":true, "saveAs":"x","locale":"es"},
    {"key":"start", "dateFormat":"YYYY-MM-DD" ,"timeFormat":"HH:mm", "saveAs":"YYYY-MM-DD HH:mm"},
  ],
  "optionsForSelect":[
      {"key":"dropdowns","options":["new","processing","rejected","completed"]},
      {"key":"checkbox","options":["Skopje","Belgrade","New York"]},
      {"key":"status","options":["just_created","confirmed","canceled"]},
      {"key":"radio","options":["no","maybe","yes"]},
      {"key":"radiotf","options":["true","false"]},
      {"key":"featured","options":["true","false"]}
  ],
  "optionsForRelation":[
      {
        //Firestore - Native
        "display": "name",
        "isValuePath": true,
        "key": "creator",
        "path": "/users",
        "produceRelationKey": false,
        "relationJoiner": "-",
        "relationKey": "type_eventid",
        "value": "name"
      },
      {
        //Firebase - Mimic function
        "display":"name",
        "key":"eventtype",
        "path":"",
        "isValuePath":false,
        "value":"name",
        "produceRelationKey":true,
        "relationJoiner":"-",
        "relationKey":"type_eventid"
      }
  ],
  "paging":{
    "pageSize": 20,
    "finite": true,
    "retainLastPage": false
  }
}

//Navigation
exports.navigation=[
    {
      "link": "/",
      "name": "Dashboard",
      "schema":null,
      "icon":"dashboard",
      "path": "",
       isIndex:true,
    },
    {
      "link": "assoc",
      "path": "aquaticsID/India/associations/KSA/clubs",
      "name": "Clubs",
      "icon":"location_on",
      "tableFields":["photo","name","email","contact_name","mobile_number"],
      "editFields":["name","address","city","pin_code","country","email","mobile_number","photo"],
      "subMenus":[]
    },
    {
      "link": "affiliate",
      "path": "aquaticsID/India/associations/KSA/clubs/{clubID}/coaches",
      "name": "Coaches",
      "icon":"accessibility",
      "tableFields":["photo","KSA_ID", "name","email","mobile_number"],
      "subMenus":[]
    },
    {
      "link": "athlete",
      "path": "main",
      "name": "Athletes",
      "icon":"pool",
      "tableFields":["photo","KSA_ID","full_name","email","mobile_number"],
      "subMenus":[
        {
          "link": "athlete",
          "path": "aquaticsID/India/associations/KSA/clubs/{clubID}/athletes/byClub",
          "name": "By Club",
          "icon":"location_on",
          "tableFields":["photo","KSA_ID","full_name","email","mobile_number"]
        },
        {
          "link": "athlete",
          "path": "aquaticsID/India/associations/KSA/clubs/{clubID}/athletes/byGroup",
          "name": "By Group",
          "icon":"group",
          "tableFields":["photo","KSA_ID","full_name","email","mobile_number"]
        }
      ]
    },
  
    {
      "link": "meets",
      "path": "meet_main",
      "name": "Meets",
      "icon":"alarm",
      // "tableFields":["photo","KSA_ID","full_name","email","mobile_number"],
      "subMenus":[
        {
          "link": "venues",
          "path": "aquaticsID/India/associations/KSA/venues",
          "name": "Venues",
          "icon":"location_on",
          "tableFields":["photo","name","address","city","pin_code","state"]
        },
        {
          "link": "meets",
          "path": "aquaticsID/India/associations/KSA/meets",
          "name": "Manage",
          "icon":"group",
          // "tableFields":["photo","KSA_ID","full_name","email","mobile_number"]
        }
      ]
    }
  ];

exports.pushSettings={
  "pushType":"expo", //firebase -  onesignal - expo
  "Firebase_AuthorizationPushKey":"AIzaSyCFUf7fspu61J9YsWE-2A-vI9of1ihtSiE", //Firebase push authorization ket
  "pushTopic":"news", //Only for firebase push
  "oneSignal_REST_API_KEY":"",
  "oneSignal_APP_KEY":"",
  "included_segments":"Active Users", //Only for onesignal push
  "firebasePathToTokens":"/expoPushTokens", //we save expo push tokens in firebase db
  "saveNotificationInFireStore":true, //Should we store the notification in firestore
}

exports.userDetails={

}

exports.remoteSetup=false;
exports.remotePath="admins/mobidonia";
exports.allowSubDomainControl=false;
exports.subDomainControlHolder="admins/";
