import React, {Component,PropTypes} from 'react'
import {default as DT} from 'react-datetime'
import 'react-datetime/css/react-datetime.css'
import moment from 'moment'


var validDOB = function( current ){
    var notValid = DT.moment().endOf('year').subtract(5, 'year');
    return current.isBefore( notValid );
};

class KreedDatePicker extends Component {

  constructor(props) {
    super(props);
    this.state = {
      value:moment(props.value,"DD/MM/YYYY")
    };
    this.handleChange=this.handleChange.bind(this);
  }

  handleChange(event) {
   this.setState({value: event});

   //Convert to desired save format 
    var converted=moment(event).format("LL")
    this.props.updateAction(this.props.theKey,converted,false,"date");
  }

  render() {
    return (
            <div className="form-group label-floating is-empty">
                <label className="control-label"></label>
                <DT inputProps={{placeholder:'DD/MM/YYYY', readOnly:true}} 
                    onChange={this.handleChange} 
                    value={this.props.value} 
                    dateFormat="DD/MM/YYYY" 
                    timeFormat={false} 
                    closeOnSelect={true}
                    isValidDate={this.validDOB}
                    locale={"en"} 
                     />
            </div>
    )
  }
}
export default KreedDatePicker;
