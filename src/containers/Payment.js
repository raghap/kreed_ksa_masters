import React, { Component, PropTypes } from 'react'
import { Link, Redirect } from 'react-router'
import Input from '../components/fields/Input.js';
import firebase from '../config/database'
import SweetAlert from 'react-bootstrap-sweetalert';
import * as firebaseREF from 'firebase';
require("firebase/firestore");
const axios = require("axios");

const HASH_SEED = "arTBk2DfvbZ0uCZD9Vij6d7Y2g33CvdfoGI6n3yEzK05yPTuA90hL6z0ddwokCDE";


class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      errorAlert: false,
      paymentSuccess: "No"
    }
    this.capturePayment = this.capturePayment.bind(this);
    // this.launchRazorPay = this.launchRazorPay.bind(this);
  }

  hash(str) {
    var hash = 5381, i = str.length;
  
      while(i) {
        hash = (hash * 33) ^ str.charCodeAt(--i);
      }
      return hash >>> 0;
  }

 
 
  componentDidMount() {
    // alert(this.props.location.query.enrollment_id + ", " + 
    // this.props.location.query.contact + ", " +
    // this.props.location.query.amount + ", " +
    // this.props.location.query.email,
    // this.props.location.query.krid);

    var chkstr = this.hash(HASH_SEED +  this.props.location.query.krid + 
      this.props.location.query.amount + this.props.location.query.enrollment_id +
      this.props.location.query.email + this.props.location.query.contact);

    if(chkstr != this.props.location.query.chk){
    this.state.errorStatement = "Invalid Payment URL";
    this.setState({ errorAlert: true });
    return;
    }


    var options = {
      "key": "rzp_live_SKW6ZcvymAaWBJ",
      "amount": this.props.location.query.amount * 100 , // 2000 paise = INR 20
      "name": "SwimIndia",
      "description": "20th State Masters Swimming Championship- 2018",
      "handler": this.capturePayment,
      "prefill": {
          "contact": this.props.location.query.contact,
          "email": this.props.location.query.email,
      },
      "notes": {
          "address": "Bangalore"
      },
      "theme": {
          "color": "#528FF0"
      }
    };

var rzp = new window.Razorpay(options);
rzp.open();

  }


  capturePayment(payResp)
  {
    this.setState({ isLoading: true });
      //alert(resp.razorpay_payment_id);
      axios({
          method: 'post',
          headers:  {
                        'Content-Type': 'application/json',
                        'Access-Control-Allow-Origin': '*',
                    },
          url: 'https://us-central1-kreed-of-sports.cloudfunctions.net/submitPaymentInfo',
          data: {Kreed_ID: this.props.location.query.krid, enrollment_id: this.props.location.query.enrollment_id,
                 payment_id: payResp.razorpay_payment_id, brand_name: "masters-ksa", amount: this.state.amount_to_pay}
      })
      .then(payResp => {
          // alert("Payment Info Submitted");
          this.state.errorStatement = "Payment Successful";
          this.setState({ isLoading: false, errorAlert: true, paymentSuccess: "Yes" });
      })
      .catch(payerr => {
          this.state.errorStatement = "Error capturing payment details";
          this.setState({ isLoading: false, errorAlert: true })
          // alert("Error: "+payerr);
      })

  }
//Container Render Method
  render() {
    return (
      
      <div className="content">
       {/* SWEET ALERTS */}
       <SweetAlert
              show={this.state.isLoading}
              title="Please wait!"
              confirmBtnText="Yes"
              confirmBtnBsStyle="danger"
              confirmBtnCssClass="loadingBtnDisplay"
            >
              <img className="img-circle" src='assets/img/spin.gif' />
        </SweetAlert>
        {/* Must select atleat one athlete */}
        <SweetAlert warning
              show={this.state.errorAlert}
              confirmBtnBsStyle="warning"
              confirmBtnCssClass="deleteAlertBtnColor"
              onConfirm={() => this.setState({ errorAlert: false })}>
              {this.state.errorStatement}
              </SweetAlert>
        <div className="container side-margin">
          {/* Image */}
          <div className="row">
            <div className="col-sm-12 padding-0-banner padding-lr-15">
              <div className="banner-img ">
              <img className="banner-img-styling" src='assets/img/20th_State_Masters_Swimming_Championship_2018_wallpaper.png' />
              </div>
            </div>
          </div>
          {/* Image */}
          {/* wrapper */}
          <div className="row whitebg border border-bottom-none">
            <div className="col-sm-12 common-padding">
              <div className="wrapper-img ">
              <img className="wrapper-img-styling" src='assets/img/20th_State_Masters_Swimming_Championship_2018_wrapper.png' />
                <h2>20th State Masters Swimming Championship- 2018</h2>
              </div>
            </div>
          </div>
          {/* wrapper */}
          {/* Payment successful */}
          {this.state.paymentSuccess == "Yes" && <div> <div className="row whitebg border padding-20">
          <div className="col-sm-12 details-padding">
            {/* <p className="margin-b-7">Dear <b>{this.state.full_name}</b> ,</p> */}
            <p className="margin-b-7">Thank you. </p>
            <p className="margin-b-7">Your payment for the registration of <b>20th State Masters Swimming Championship- 2018,</b> was successful.</p>
            <p className="margin-b-7">An acknowledgement for the registration has been sent to your email.</p>
            <p className="margin-b-7">We look forward to meet you at the venue.</p>
            <p className="margin-b-7">For any queries, please contact +91 91084 56704 or email us at registrations@swimindia.in</p>
          </div>
          </div>
          </div>}
          {/* Payment successful */}
        </div>
      </div>
    )
  }
}
export default App;
