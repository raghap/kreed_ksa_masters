import React, { Component, PropTypes } from 'react'
import { Link, Redirect } from 'react-router'
import Input from '../components/fields/Input.js';
import firebase from '../config/database'
import KreedDatePicker from '../components/fields/KreedDatePicker.js';
import ClubSelect from '../components/fields/ClubSelect.js';
import CheckBox from '../components/fields/CheckBox.js';
import Radio from '../components/fields/Radio.js'
import moment from 'moment';
import SweetAlert from 'react-bootstrap-sweetalert';
import SkyLight from 'react-skylight';
import FileUploader from 'react-firebase-file-uploader';
import KreedImage from '../components/fields/KreedImage.js';
import Indicator from '../components/Indicator'
import * as firebaseREF from 'firebase';
require("firebase/firestore");
const axios = require("axios");

// for file uploader
const uuidv1 = require('uuid/v1');

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      meet_name:"20th State Masters Swimming Championship- 2018",
      Kreed_ID:"",
      full_name: "",
      registered_status:"yes",
      date_of_birth: "",
      blood_group: "",
      gender: "",
      mobile_number: "",
      email: "",
      parent_name: "",
      parent_mobile_number: "",
      city: "",
      club_name: "",
      clubList: ["Abba Sports Club","Aqua Swimming Centre","Army Public School","Association of Integrated Mysore Swimmers AIMS",
      "Baldwin Boy’S High School","Baldwin Girl'S High School","Baldwin Girl'S High School","Bangalore Club",
      "Bangalore International School","Bangalore Swimming Research Centre","Basava Rajeshwari Public School",
      "Basavanagudi Aquatic Centre","Bishop Cotton'S Boys High School","Carmel School","Christ Academy",
      "Chrysalis High","Concorde International","Davanagere Swimming Aquatics","Dolphin Aquatics",
      "DPS","Edify School","Ekalavya","Frank Anthony Public School","G.B.M.M. High School","Glitzy Swimming Centre",
      "Global City International School","Global Swim Centre","Glorious Aquatics","Golden Fins Sports Club",
      "Greenwood High International","Gulbarga Aqua Association","Gurukul Sport","Indus International School",
      "Inventure Academy","Jai Hind Swimming Club","JSS Public School","Kempegowda Swimming Pool",
      "Kendriya Vidyalaya Malleshwaram","Lakeshore Swimming Club","LR English School","LSH Club","M K Sports Club",
      "Mahila Seva Samaja","Mallya Adithi International School","Mangala Swimming Club","Mangalore Aquatic Club",
      "Matsya Aquatics","MG Sports","Michael Phelps Swimming School","Mysore Dist Swimming Association",
      "Naresh Swimming Academy","National Publc School","National Public School Kengeri","Neev Academy",
      "Nettakallappa Aquatic Center","Nisha Millet Swimming Academy","NMSA Marlins","Orchids International School",
      "Passionate Sports Academy","PET Aquatic Centre","Pooja Aquatic Centre","Poornaprajna Education Centre",
      "Presidency School","Pupil Tree School","Puttur Aquatic Club","Rashtrotthana Vidya Kendra",
      "Ray Centre","Ryan International School","Shishu Griha School","Shivamogga Sports Complex",
      "Shri Kumarans High School","Shri Manjunatheshwara Central School","SIMS Aquatics",
      "SIMS Aquatics","Sophia High School","Soundarya Aquatic Centre","Spark Aquatics",
      "Splash Aquatics","Spoorthi Aquatic Gulbarga","Sprint Swimming Academy","Sri Sharada Vidyanikethan",
      "Srujan Aquatics","Star Academy","Star Aquatic Club","Star Fish Academy","Sudarshan Vidya Mandir",
      "SVM International School","SwimIndia","SwimLife Swimming Academy","Swimmers Club Belgaum",
      "The Brigade School","The Foundation School","TISB","Vibgyor High","Vidya Sanskar International School",
      "Vidyanikethan","Vidyashilp Academy","Vijayanagar Aquatic Centre","Vishwabharathi Public School",
      "Young Challengers Swim CLub","Zee Swim Academy","Others"],
      other_club_name:"",
      swimGroup: "",
      athleteInfo: "",
      //fees
      registration_fee: 400,
      admission_fee: 200,
      events_fee: 0,
      amount_to_pay: 600,

      events:"",
      eventList: [],
      selectedEvents:[],
      selectedEventTimings:[],
      // relay events
      // relay_event:"",
      // relayEventList: [],
      // relay_team: "",
      // selectedRelayEvent: [],
            
      agree_TC: "",
      selectedClubValue:"",
      isLoading: false,
      errorAlert: false,
      // athleteLoadingError: false,
      athleteObj:{},
      newKreedID:"",
      enrollmentID:"",
      // Navigation for different pages
      register: "Yes",
      thankyou: "No",
      paymentSuccess: "No",
      // Documents
      docURL: "",
      isUploading: false,
      dlProgress: new Map(),
      // Photos
      photo: "https://firebasestorage.googleapis.com/v0/b/kreed-of-sports.appspot.com/o/photos%2Fdefault_profile_350x400.png?alt=media&token=0741bfbb-8f54-478e-9339-32e520d66c92",
      imageLoading: false
    }

    var strMeetName = this.state.meet_name.replace(/[^A-Z0-9]+/ig, "_");
    this.state.docRef = firebase.app.storage().refFromURL('gs://kreed-of-sports-ksa-masters/' + strMeetName + "/" + uuidv1());

    this.handleInputChange = this.handleInputChange.bind(this);
    this.updateAction = this.updateAction.bind(this);
    this.getGroup = this.getGroup.bind(this);
    this.getEventList = this.getEventList.bind(this);
    // this.getRelayEventList = this.getRelayEventList.bind(this);
    this.resetdata = this.resetdata.bind(this);
    this.submitRegistration = this.submitRegistration.bind(this);
    this.capturePayment = this.capturePayment.bind(this);
    this.getInputFields = this.getInputFields.bind(this);
    this.displayRegisteredEvents = this.displayRegisteredEvents.bind(this);
    this.payOnline = this.payOnline.bind(this);
   //for displaying docs list
    this.displayList = this.displayList.bind(this);
    this.handleUploadStart = this.handleUploadStart.bind(this);
    this.handleProgress = this.handleProgress.bind(this);
    this.handleUploadError = this.handleUploadError.bind(this);
    this.handleUploadSuccess = this.handleUploadSuccess.bind(this);
    // Uploading images
    this.markUploaderStart = this.markUploaderStart.bind(this);
    this.cancelphotoGuidelines = this.cancelphotoGuidelines.bind(this);
  }

  // for diplaying file list
  handleUploadStart(fname, task) {
    this.state.dlProgress.set(task.snapshot.ref.fullPath, 0);
    if (!this.state.isUploading)
      this.setState({ isUploading: true, progress: 0 });
  }
  handleProgress(progress, task) {
    if (progress <= 100) {
      this.state.dlProgress.set(task.snapshot.ref.fullPath, progress)
      this.setState({ avatarURL: task.snapshot.ref.fullPath });
    }
  }
  handleUploadError(error) {
    this.setState({ isUploading: false });
    console.error(error);
  }
  handleUploadSuccess(filename, task) {
    // this.setState({avatar: filename, progress: 100, isUploading: false})
    this.state.dlProgress.delete(task.snapshot.ref.fullPath);
    if (this.state.dlProgress.size == 0)
      this.state.isUploading = false;
      this.state.docRef.child(filename).getDownloadURL()
      .then(url => {
        this.setState({ docURL: url });
      })
      .catch(error => {
        alert("err "+error)
      })
  };

   //displaying file names
   displayList() {
    var i;
    var items = [];

    var link = this.state.docURL;
    var pts = link.split('/');
    var fn = unescape(pts[pts.length - 1].split('?')[0]);
    var fnparts = fn.split('/');
    items.push(fnparts[2]);
    
    return items;
  }
  // image loading start
  markUploaderStart() {
    this.state.imageLoading = true;
  }
  
  cancelphotoGuidelines() {
    this.refs.photoGuidelines.hide();
  }
  resetdata() {
    var newState = {};
      newState.full_name = "";
      newState.second_name = "";
      newState.date_of_birth = "",
      newState.gender = "",
      newState.mobile_number = "",
      newState.email = "",
      newState.parent_name = "",
      newState.parent_mobile_number = "",
      newState.events = "",
      newState.city = "",
      newState.club_name = "",
      newState.selectedEvents = [],
      newState.selectedEventTimings = [],
      newState.ksa_id = "",
      newState.eventList = [],
      newState.selectedClubValue = ""
    this.setState(newState);

  }
  getEventList() {
    var events = [];
    if (this.state.swimGroup == "Group A" || this.state.swimGroup == "Group B" || 
        this.state.swimGroup == "Group C" || this.state.swimGroup == "Group D" || 
        this.state.swimGroup == "Group E" || this.state.swimGroup == "Group F" ||
        this.state.swimGroup == "Group G" || this.state.swimGroup == "Group H" ||
        this.state.swimGroup == "Group I" || this.state.swimGroup == "Group J" ||
        this.state.swimGroup == "Group K" || this.state.swimGroup == "Group L") 
      {
      events = ["50m Freestyle", "100m Freestyle", "200m Freestyle", "400m Freestyle", "50m Backstroke",
                "100m Backstroke", "50m Breast Stroke", "100m Breast Stroke", "50m Butterfly",
                "100m Butterfly", "200m IM"];
      }  
      this.setState({ eventList: events })
  }
  // getRelayEventList(){
  //   var relayEvents = [];
  //   if (this.state.swimGroup == "Group A" || this.state.swimGroup == "Group B" || 
  //       this.state.swimGroup == "Group C" || this.state.swimGroup == "Group D" || 
  //       this.state.swimGroup == "Group E" || this.state.swimGroup == "Group F" ||
  //       this.state.swimGroup == "Group G" || this.state.swimGroup == "Group H" ||
  //       this.state.swimGroup == "Group I" || this.state.swimGroup == "Group J" ||
  //       this.state.swimGroup == "Group K" || this.state.swimGroup == "Group L") 
  //    {
  //     relayEvents = ["4x50m Freestyle Relay", "4x50m Med Relay"];
  //    }
  //   this.setState({ relayEventList: relayEvents })
  // }
  // to get group
  getGroup(dob) {
    var timestamp = Date.parse(dob);

    if (isNaN(timestamp))
      return "Open";

    var current = new Date();
    var dob_dt = new Date(dob);
    var today = current.getFullYear() - dob_dt.getFullYear();

    if (today > 80) {
      return "Group L";
    } else if (today == 25 || today == 26 ||today == 27 || today == 28 || today == 29) {
      return "Group A";
    } else if (today == 30 || today == 31 || today == 32 || today == 33 || today == 34) {
      return "Group B";
    } else if (today == 35 || today == 36 || today == 37 || today == 38 || today == 39) {
      return "Group C";
    } else if (today == 40 || today == 41 || today == 42 || today == 43 || today == 44) {
      return "Group D";
    } else if (today == 45 || today == 46 || today == 47 || today == 48 || today == 49) {
      return "Group E";
    } else if (today == 50 || today == 51 || today == 52 || today == 53 || today == 54) {
      return "Group F";
    } else if (today == 55 || today == 56 || today == 57 || today == 58 || today == 59) {
      return "Group G";
    } else if (today == 60 || today == 61 || today == 62 || today == 63 || today == 64) {
      return "Group H";
    } else if (today == 65 || today == 66 || today == 67 || today == 68 || today == 69) {
      return "Group I";
    } else if (today == 70 || today == 71 || today == 72 || today == 73 || today == 74) {
      return "Group J";
    } else if (today == 75 || today == 76 || today == 77 || today == 78 || today == 79) {
      return "Group K";
    } else {
      return "Below 25 years of age are not permitted."
    }
  }
  componentDidMount() {
   
  }

  handleInputChange(event) {
    const target = event.target;
    const name = target.name;
    const value = target.value;

    if (name == "full_name") {
      if (!value.match(/^[ a-zA-Z.]+$/) && value != '') {
        return false;
      }
    }

    if (name == "other_club_name") {
      if (!value.match(/^[ a-zA-Z.']+$/) && value != '') {
        return false;
      }
    }

    if (name == "parent_name") {
      if (!value.match(/^[ a-zA-Z.]+$/) && value != '') {
        return false;
      }
    }
   
    
    if (name == "mobile_number") {
      if (!value.match(/^[0-9]+$/) && value != '') {
        return false;
      }
      if (value.length > 10) {
        return false;
      }
    }
    if (name == "parent_mobile_number") {
      if (!value.match(/^[0-9]+$/) && value != '') {
        return false;
      }
      if (value.length > 10) {
        return false;
      }
    }
   

    if (name == "pin_code") {
      if (!value.match(/^[0-9]+$/) && value != '') {
        return false;
      }
      if (value.length > 6) {
        return false;
      }
    }

    if (name == "max_events") {
      if (!value.match(/^[0-9]+$/) && value != '') {
        return false;
      }
    }

    if(name.startsWith("eventTiming")){
      var index = name.split("_")[1];
      var timings = this.state.selectedEventTimings;
      timings[index] = value;
      this.setState({selectedEventTimings: timings});
      return;
    }

    this.setState({
      [name]: value
    });
  }
  processValueToSave(value, type) {
    //To handle number values
    if (!isNaN(value)) {
      value = Number(value);
    }

    //To handle boolean values
    value = value === "true" ? true : (value === "false" ? false : value);


    if (type == "date") {
      //To handle date values
      if (moment(value).isValid()) {
        value = moment(value).toDate();
        //futureStartAtDate = new Date(moment().locale("en").add(1, 'd').format("MMM DD, YYYY HH:MM"))
      }
    }

    return value;
  }
  updateAction(key, value, dorefresh = false, type = null, forceObjectSave = false) {
    value = this.processValueToSave(value, type);
    var firebasePath = (this.props.route.path.replace("/assoc/", "").replace(":sub", "")) + (this.props.params && this.props.params.sub ? this.props.params.sub : "").replace(/\+/g, "/");
    if (this.state.theSubLink != null && !forceObjectSave) {
      this.updatePartOfObject(key, value, dorefresh, type, firebasePath)
    } else {

      //value=firebase.firestore().doc("/users/A2sWwzDop0EAMdfxfJ56");
      //key="creator";

      console.log("firebasePath from update:" + firebasePath)
      console.log('Update ' + key + " into " + value);

      if (key == "NAME_OF_THE_NEW_KEY" || key == "VALUE_OF_THE_NEW_KEY") {
        console.log("THE_NEW_KEY")
        var updateObj = {};
        updateObj[key] = value;
        this.setState(updateObj);
        console.log(updateObj);
      } else {

        if (key == "date_of_birth") {
          this.state.selectedEvents = "";
          this.state.events = "";
          // this.state.relay_event = ""
          this.state.swimGroup = this.getGroup(value);
          this.getEventList();
          // this.getRelayEventList();
        }

        //amount to pay for new registration
        if(key == "registered_status"){
         if(value == "no"){
            this.state.registration_fee = 0;
            this.state.admission_fee = 200;
            this.state.amount_to_pay = this.state.registration_fee + this.state.admission_fee;
          }else  if(value == "yes"){
            this.state.registration_fee = 400;
            this.state.admission_fee = 200;
            this.state.amount_to_pay = this.state.registration_fee + this.state.admission_fee;
          }
        }

        //Checkbox handling
        if(key == "events"){
        
          if(typeof(value) == "string")
            this.state.selectedEvents = value.slice(1).split(",");
          else
          {
            this.state.selectedEvents = [];
            value = "";
            this.state.events_fee = 0;
          }
        }
        
        // if(key == "relay_event") {
        //     if(typeof(value) == "string")
        //     {
        //       this.state.selectedRelayEvent = value.slice(1).split(",");   
        //       this.state.amount_to_pay = "850";
        //     }
        //     else
        //     {
        //       this.state.selectedRelayEvent = [];
        //       this.state.amount_to_pay = "600";
        //       value = "";
        //     }
    
        // }

        // handling photo
        if (key == "photo") {
          this.state.imageLoading = false;
        }

        this.setState({ [key]: value });
      }

    }
  }
  getInputFields(){
    var i;
    var items = [];
    var event_name = [];
    var selectedEvtTime = []
    for (i = 0; i < this.state.selectedEvents.length; i++) {
      items.push(
      <div className="row">
      <div className="col-sm-4">
        <h4 className="eventname-styling">
        {this.state.selectedEvents[i]}
        </h4>
      </div>
      <div className="col-sm-6">
       <input type="text" placeholder="00:00.00" name={ "eventTiming_" + i} aria-required="true" value={this.state.selectedEventTimings[i]} onChange={this.handleInputChange} className="form-control" />
      </div>
    </div> 
    
      );
      this.state.events_fee = 100 * (i+1);
      this.state.amount_to_pay = this.state.registration_fee + this.state.admission_fee + this.state.events_fee;
    }
    return items;
  }
  displayRegisteredEvents(){
    var i;
    var items = [];

    for (i = 0; i <  this.state.selectedEvents.length; i++) {
      items.push(<li>{this.state.selectedEvents[i]}</li>);
    }
    return items;
  }



  getDataObject()
  {
      var dObj = {};

        if(this.state.city == ""){
          this.state.city = "Bangalore";
        }

        dObj.full_name = this.state.full_name;
        dObj.date_of_birth = this.state.date_of_birth;
        dObj.gender = this.state.gender;
        
        dObj.parent_name = this.state.parent_name;
        dObj.swimGroup = this.state.swimGroup;
        dObj.city = this.state.city;
        dObj.country = "India";
        dObj.blood_group = this.state.blood_group;

        //common fields
        dObj.mobile_number = this.state.mobile_number;
        dObj.email = this.state.email;
        dObj.parent_mobile_number = this.state.parent_mobile_number;
        dObj.club_name = this.state.club_name;
        dObj.amount = this.state.amount_to_pay;

        //event regn fields - always carried.
        dObj.brand_name = "masters-ksa";
        dObj.meet_name = this.state.meet_name;
        dObj.selected_events = this.state.selectedEvents.slice();
        dObj.selected_event_timings = this.state.selectedEventTimings.slice();
        dObj.document_path = this.state.docURL;
        dObj.photo = this.state.photo;

    return dObj;

  }

  capturePayment(payResp)
  {
    this.setState({ isLoading: true });
      //alert(resp.razorpay_payment_id);
      axios({
          method: 'post',
          headers:  {
                        'Content-Type': 'application/json',
                        'Access-Control-Allow-Origin': '*',
                    },
          url: 'https://us-central1-kreed-of-sports.cloudfunctions.net/submitPaymentInfo',
          data: {Kreed_ID: this.state.newKreedID, enrollment_id: this.state.enrollmentID, payment_id: payResp.razorpay_payment_id, brand_name: "masters-ksa", amount: this.state.amount_to_pay}
      })
      .then(payResp => {
          // alert("Payment Info Submitted");
          this.state.errorStatement = "Payment Successful";
          this.setState({ isLoading: false, errorAlert: true ,thankyou: "No",paymentSuccess: "Yes"  });
      })
      .catch(payerr => {
          this.state.errorStatement = "Error capturing payment details";
          this.setState({ isLoading: false, errorAlert: true })
          // alert("Error: "+payerr);
      })

  }

  payOnline(){
    var options = {
      "key": "rzp_live_SKW6ZcvymAaWBJ",
      "amount": this.state.amount_to_pay * 100 , // 2000 paise = INR 20
      "name": "SwimIndia",
      "description": "20th State Masters Swimming Championship- 2018",
      // "image": "/your_logo.png",
      "handler": this.capturePayment,
      "prefill": {
          "contact": this.state.mobile_number,
          "email": this.state.email,
      },
      "notes": {
          "address": this.state.city
      },
      "theme": {
          "color": "#528FF0"
      }
    };
    var rzp = new window.Razorpay(options);
    rzp.open();

  }
  submitRegistration(e)
  {
      e.preventDefault();
     
      if (this.state.imageLoading ) {
        this.state.errorStatement = "Please wait image is uploading";
        this.setState({errorAlert: true})
        return;
      }

      if(this.state.isUploading){
        this.state.errorStatement = "Please wait Uploading your document";
        this.setState({errorAlert: true})
        return;
      }

      if(this.state.date_of_birth == "" ){
        this.state.errorStatement = "Select Your Date Of Birth";
        this.setState({errorAlert: true})
        return;
      }

      if(this.state.docURL == "" ){
        this.state.errorStatement = "Please upload your DOB proof";
        this.setState({errorAlert: true})
        return;
      }

      if(this.state.blood_group == "" ){
        this.state.errorStatement = "Please ensure you have selected your blood group";
        this.setState({errorAlert: true})
        return;
      }

      if(this.state.gender === "" ){
        this.state.errorStatement = "Please ensure you have selected a gender";
        this.setState({errorAlert: true})
        return;
      }

      if(this.state.mobile_number.length < 10 ){
        this.state.errorStatement = "Please ensure you have entered a 10 digit mobile number";
        this.setState({errorAlert: true})
        return;
      }

      if(this.state.club_name == ""){
        this.state.errorStatement = "Select Your Club";
        this.setState({errorAlert: true})
        return;
      }

      if(this.state.club_name == "Others" && this.state.other_club_name == ""){
        this.state.errorStatement = "Enter Your Club name";
        this.setState({errorAlert: true})
        return;
      }

      if(this.state.selectedEvents.length < 1 ){
        this.state.errorStatement = "Select atleast one event";
        this.setState({errorAlert: true})
        return;
      }

      if(this.state.selectedEvents.length >= 5 ){
        this.state.errorStatement = "You can select a maximum of 4 events";
        this.setState({errorAlert: true})
        return;
      }

      if (this.state.photo == "https://firebasestorage.googleapis.com/v0/b/kreed-of-sports.appspot.com/o/photos%2Fdefault_profile_350x400.png?alt=media&token=0741bfbb-8f54-478e-9339-32e520d66c92") {
        this.state.errorStatement = "You must upload an ID card photo by clicking on the photo control and then hit the green checkmark to apply";
        this.setState({errorAlert: true})  
        return;
      }

      if(this.state.club_name == "Others"){
        this.state.club_name = this.state.other_club_name
      }

      this.setState({ isLoading: true });
      axios({
        method: 'post',
        headers:  {
                      'Content-Type': 'application/json',
                      'Access-Control-Allow-Origin': '*',
                  },
        url: 'https://us-central1-kreed-of-sports.cloudfunctions.net/submitMeetRegistration',
        data: this.getDataObject()
      })
      .then(response => {
        // alert("KreedId" + response.data.kreedID + ", Enrolment Id: " + response.data.enrollmentID);
        this.state.newKreedID = response.data.kreedID;
        this.state.enrollmentID = response.data.enrollmentID;
        // Navigating to thankyou PAGE
        this.setState({register: "No" , thankyou: "Yes",isLoading: false });
      })
      .catch(error => {
        this.state.errorStatement = "Error completing registration";
        this.setState({ isLoading: false, errorAlert: true })
        // console.log("Error submitting athlete details: ", error);
      })
 }


//Container Render Method
render() {
  return (
       <img alt="registration are closed" src='assets/img/registration_closed.png' width="1510px" />
  )
}
  render_unused() {
        //styling for add Photo Guidelines skylight
        var photoGuidelines = {
          height: 'auto',
          position: 'absolute',
          padding: '0px'
        };
    return (
      
      <div className="content">
      {/* SWEET ALERTS */}
      <SweetAlert
              show={this.state.isLoading}
              title="Please wait!"
              confirmBtnText="Yes"
              confirmBtnBsStyle="danger"
              confirmBtnCssClass="loadingBtnDisplay"
            >
              <img className="img-circle" src='assets/img/spin.gif' />
        </SweetAlert>
        {/* Must select atleat one athlete */}
        <SweetAlert warning
              show={this.state.errorAlert}
              confirmBtnBsStyle="warning"
              confirmBtnCssClass="deleteAlertBtnColor"
              onConfirm={() => this.setState({ errorAlert: false })}>
              {this.state.errorStatement}
              </SweetAlert>
        <div className="container side-margin">
          {/* Image */}
          <div className="row">
            <div className="col-sm-12 padding-0-banner padding-lr-15">
              <div className="banner-img ">
                <img className="banner-img-styling" src='assets/img/20th_State_Masters_Swimming_Championship_2018_wallpaper.png' />
              </div>
            </div>
          </div>
          {/* Image */}
          {/* wrapper */}
          <div className="row whitebg border border-bottom-none">
            <div className="col-sm-12 common-padding">
              <div className="wrapper-img ">
                <img className="wrapper-img-styling" src='assets/img/20th_State_Masters_Swimming_Championship_2018_wrapper.png' />
                <h2>{this.state.meet_name}</h2>
              </div>
            </div>
          </div>
          {/* wrapper */}
          {/* Main Form */}
          {this.state.register == "Yes" && <div> <div className="row whitebg border padding-20">
            {/* Address */}
             <div className="col-sm-12 common-padding">
              <div className="col-sm-12">
                <p className="margin-0px"><b>Venue: </b>Vijayanagar Aquatic Centre, Corporation Swimming Pool, RPC Layout, Banglaore</p>
                <p className="margin-0px"><a target="_blank" className="linkStyling text-color" href="https://goo.gl/maps/r7vjehcprS72"><b>Location Map</b></a></p>
                <p className="margin-0px"><b>Dates: </b> 1st Sept & 2nd Sept, 2018 </p>
                <p className="margin-0px"><b>Competition Start Time: </b>9:00am</p>
                Visit <a target="_blank" className="linkStyling" href="http://swimindia.in/20th-karnataka-state-masters-swimming-championships-2018-bangalore">SwimIndia</a> to know more about the meet.
              </div>
            </div>
            {/* Address */}
            {/* Form */}
            <form onSubmit={this.submitRegistration}>
            <div className="row">
            <div className="col-sm-6 common-padding">
               {/* registered or not */}
               <div className="col-sm-12">
                <label className="control-label label-color">Is this your first time particiation?</label>
              </div>
              <div className="col-sm-8 margin-padding-0">
                <div className="input-group display-block">
                  <div className="form-group label-floating ">
                   <Radio theKey="registered_status"  required="true" updateAction={this.updateAction} value={this.state.registered_status} options={["yes", "no"]} />
                  </div>
                </div>
              </div>
              {/* registered or not */}
              {/* First Name */}
              <div className="col-sm-12">
                <label className="control-label label-color">Swimmer Name <span className="color-red"> *</span></label>
              </div>
              <div className="col-sm-8 margin-padding-0">
                <div className="input-group display-block">
                  <div className="form-group label-floating ">
                   <input type="text" required="true" name="full_name" aria-required="true" value={this.state.full_name} onChange={this.handleInputChange} className="form-control" />
                  </div>
                </div>
              </div>
              {/* First Name */}
              {/* Date of birth */}
              <div className="col-sm-12">
                <label className="control-label label-color">Date Of Birth <span className="color-red"> *</span></label>
              </div>
              <div className="col-sm-8 margin-padding-0">
                <div className="input-group display-block">
                  <div className="form-group label-floating ">
                     <KreedDatePicker theKey="date_of_birth" required="true" value={this.state.date_of_birth} updateAction={this.updateAction} />
                  </div>
                </div>
              </div>
              {/* Date of birth */}
              {/* Documents */} 
                <div className="col-sm-12">
                  <label className="control-label label-color" >Upload DOB Proof <span className="color-red"> *</span></label>
                </div>
                <div className="col-sm-8 margin-padding-0">
                  <div className="input-group display-block">
                    <div className="form-group label-floating">
                      {/* {this.state.isUploading &&
                              <p>Progress: {this.state.dlProgress}</p>
                            } */}
                      <div className="list-styling">
                        {this.displayList()}
                      </div>
                      <div><span style={{ color: "red", fontSize: 12 }}>Please do not upload your Aadhar card copy</span></div>
                      <label style={{ backgroundColor: '#1D88D4', color: 'white', padding: 10, borderRadius: 4, pointer: 'cursor' }}>
                        Select File
                        <FileUploader
                          hidden
                          accept=".gif, .jpg, .png, .pdf, .doc, .docx"
                          name="attachments"
                          storageRef={this.state.docRef}
                          onUploadStart={this.handleUploadStart}
                          onUploadError={this.handleUploadError}
                          onUploadSuccess={this.handleUploadSuccess}
                          onProgress={this.handleProgress}
                        />
                      </label>
                      <Indicator show={this.state.isUploading} />
                    </div>
                  </div>
                </div>
              {/* Documents */}

              {/* Blood Group */}
              <div className="col-sm-12">
                <label className="control-label label-color">Blood Group <span className="color-red"> *</span></label>
              </div>
              <div className="col-sm-8 margin-padding-0">
                <div className="input-group display-block">
                  <div className="form-group label-floating ">
                    <ClubSelect theKey="blood_group" updateAction={this.updateAction} value={this.state.blood_group} options={["O+", "O-", "A+", "A-", "B+", "B-", "AB+", "AB-"]} /> 
                  </div>
                </div>
              </div>
              {/* Blood Group */}

              {/* Gender */}
              <div className="col-sm-12">
                <label className="control-label label-color">Gender <span className="color-red"> *</span></label>
              </div>
              <div className="col-sm-8 margin-padding-0">
                <div className="input-group display-block">
                  <div className="form-group label-floating ">
                   <Radio theKey="gender"  required="true" updateAction={this.updateAction} value={this.state.gender} options={["male", "female"]} />
                  </div>
                </div>
              </div>
              {/* Gender */}
              {/* Phone */}
              <div className="col-sm-12">
                <label className="control-label label-color">Phone <span className="color-red"> *</span></label>
              </div>
              <div className="col-sm-8 margin-padding-0">
                <div className="input-group display-block">
                  <div className="form-group label-floating ">
                    <input type="text" name="mobile_number" required="true" value={this.state.mobile_number} onChange={this.handleInputChange} className="form-control" />
                  </div>
                </div>
              </div>
              {/* Phone */}
              {/* Email */}
              <div className="col-sm-12">
                <label className="control-label label-color">Email <span className="color-red"> *</span></label>
              </div>
              <div className="col-sm-8 margin-padding-0">
                <div className="input-group display-block">
                  <div className="form-group label-floating ">
                    <input type="email" required="true" name="email" aria-required="true" value={this.state.email} onChange={this.handleInputChange} className="form-control" />
                  </div>
                </div>
              </div>
              {/* Email */}
              {/* Parents name */}
              <div className="col-sm-12">
                <label className="control-label label-color">Parent/Guardian Name <span className="color-red"> *</span></label>
              </div>
              <div className="col-sm-8 margin-padding-0">
                <div className="input-group display-block">
                  <div className="form-group label-floating ">
                   <input type="text" required="true" name="parent_name" aria-required="true" value={this.state.parent_name} onChange={this.handleInputChange} className="form-control" />
                  </div>
                </div>
              </div>
              {/* Parents name */}
              {/* Parent contact number */}
              <div className="col-sm-12">
                <label className="control-label label-color">Parent/Guardian Contact Number</label>
              </div>
              <div className="col-sm-8 margin-padding-0">
                <div className="input-group display-block">
                  <div className="form-group label-floating ">
                    <input type="text" name="parent_mobile_number" value={this.state.parent_mobile_number} onChange={this.handleInputChange} className="form-control" />
                  </div>
                </div>
              </div>
              {/* Parent contact number */}
              {/* city */}
              <div className="col-sm-12">
                <label className="control-label label-color">City</label>
              </div>
              <div className="col-sm-8 margin-padding-0">
                <div className="input-group display-block">
                  <div className="form-group label-floating ">
                   <input type="text"  name="city" aria-required="true" value={this.state.city} onChange={this.handleInputChange} className="form-control" />
                  </div>
                </div>
              </div>
              {/* city */}
              {/* Club/School */}
              <div className="col-sm-12">
                <label className="control-label label-color">Club/School<span className="color-red"> *</span></label>
              </div>
              <div className="col-sm-8 margin-padding-0">
                <div className="input-group display-block">
                  <div className="form-group label-floating ">
                    <ClubSelect theKey="club_name" updateAction={this.updateAction} value={this.state.club_name} options={this.state.clubList}/>
                  </div>
                </div>
              </div>
              {/* Club/School */}
              {/* Enter Club name manually */}
                {this.state.club_name == "Others" && <div>
                <div className="col-sm-12">
                <label className="control-label label-color">Enter Your club/school name<span className="color-red"> *</span></label>
              </div>
              <div className="col-sm-8 margin-padding-0">
                <div className="input-group display-block">
                  <div className="form-group label-floating ">
                   <input type="text"  name="other_club_name" aria-required="true" value={this.state.other_club_name} onChange={this.handleInputChange} className="form-control" />
                  </div>
                </div>
              </div>
                  </div>}
              {/* Enter Club name manually */}
              {/* Group and Gender Details*/}
              {this.state.swimGroup != "" && <div>
                <div className="col-sm-12">
                  <label className="control-label label-color"> {this.state.swimGroup}</label>
                </div>
                <div className="col-sm-12">
                  <label className="control-label label-color"> Select Events</label>
                </div>
                <div className="col-sm-12 margin-padding-0">
                  <div className="input-group display-block">
                    <div className="form-group label-floating ">
                      <CheckBox theKey="events" updateAction={this.updateAction} value={this.state.events} options={this.state.eventList} />
                    </div>
                  </div>
                </div>
                 {/* Timings for each events */}
              {this.state.selectedEvents.length >= 5 && <div>
                <div className="col-sm-12">
                  <label className="control-label color-red">You can only choose upto 4 choice(s).</label>
                </div>
                </div>}
              {(this.state.selectedEvents.length > 0 && this.state.selectedEvents.length < 5) && <div>
               {(this.state.swimGroup != "Group V" && this.state.swimGroup != "Group VI") && <div> <div className="col-sm-12">
                  <label className="control-label label-color">Enter Timing for Selected Events </label>
                </div>
                <div className="col-sm-12 margin-padding-0">
                  <div className="input-group display-block">
                    <div className="form-group label-floating ">
                      {this.getInputFields()}
                    </div>
                  </div>
                </div>
                </div>}
                </div>}
              {/* Timings for each events */}
                {/* <div className="col-sm-12">
                  <label className="control-label label-color"> Select Relay Event</label>
                </div>
                <div className="col-sm-12 margin-padding-0">
                  <div className="input-group display-block">
                    <div className="form-group label-floating ">
                      <CheckBox theKey="relay_event" updateAction={this.updateAction} value={this.state.relay_event} options={this.state.relayEventList} />
                    </div>
                  </div>
                </div> */}
                {/* Relay Members */}
               {/* {this.state.selectedRelayEvent.length > 0 && <div>
                <div className="col-sm-12">
                  <label className="control-label label-color">Enter your Relay Team Members</label>
                </div>
                <div className="col-sm-8 margin-padding-0">
                  <div className="input-group display-block">
                    <div className="form-group label-floating ">
                      <input type="text"  name="relay_team" aria-required="true" value={this.state.relay_team} onChange={this.handleInputChange} className="form-control" />
                      <span>4x50m Mixed Freestyle Relay- To take part in the mixed freestyle relay you must have four participants in each team, each team uses two boys and two girls in any order. Each team can enter no more than two relay teams in this event.</span>
                    </div>
                  </div>
                </div>
                </div>} */}
              {/* Relay Members */}
              </div>}
              {/* Group and Gender Details*/}
              {/* Registration Fee */}
              <div className="col-sm-12">
                <label className="control-label label-color">Registration Fee to be paid (In Rupees)</label>
              </div>
              <div className="col-sm-8 margin-padding-0">
                <div className="input-group display-block">
                  <div className="form-group label-floating ">
                    <p className="payment-styling"> {this.state.registration_fee} </p>
                  </div>
                </div>
              </div>
              {/* Registration Fee */}
              {/* Admission Fee */}
               <div className="col-sm-12">
                <label className="control-label label-color">Admission Fee to be paid (In Rupees)</label>
              </div>
              <div className="col-sm-8 margin-padding-0">
                <div className="input-group display-block">
                  <div className="form-group label-floating ">
                    <p className="payment-styling"> {this.state.admission_fee} </p>
                  </div>
                </div>
              </div>
              {/* registered event Fee */}
              <div className="col-sm-12">
                <label className="control-label label-color">Entry fee (Rs.100 per event)</label>
              </div>
              <div className="col-sm-8 margin-padding-0">
                <div className="input-group display-block">
                  <div className="form-group label-floating ">
                    <p className="payment-styling"> {this.state.events_fee} </p>
                  </div>
                </div>
              </div>
              {/* registered event Fee */}
              {/* Payment */}
              <div className="col-sm-12">
                <label className="control-label label-color">Total fee to be paid (In Rupees)</label>
              </div>
              <div className="col-sm-8 margin-padding-0">
                <div className="input-group display-block">
                  <div className="form-group label-floating ">
                    <p className="payment-styling"> {this.state.amount_to_pay} </p>
                  </div>
                </div>
              </div>
              {/* Payment */}
              </div>
              {/* Photo */}
              <div className="col-sm-6">
              <div className="row">
                <div className="col-sm-2"></div>
                <div className="col-sm-6">
                <button type="button" className="btn btn-link btn-twitter" onClick={() => { this.refs.photoGuidelines.show() }}>
                    <i className="material-icons">info_outline</i> Photo Guidelines
                </button>
                </div>
                <div className="col-sm-2"></div>
              </div>
              <div className="col-sm-12">
                  <label className="control-label label-color">Swimmer ID Photo<span className="color-red"> *</span></label>
              </div>
              <div className="col-sm-8 margin-padding-0">
                <div className="input-group display-block">
                  <div className="form-group label-floating ">
                    <KreedImage theKey="photo" setLoading={this.markUploaderStart} value={this.state.photo} className="profile-image" text="Upload ID Card Photo" updateAction={this.updateAction} />
                  </div>
                </div>
              </div>
              </div>
              {/* Photo */}

            </div>
              {/* Terms and Conditions */}
              <div className="col-sm-12 ">
                <label className="control-label label-color">Terms and Conditions <span className="color-red"> *</span></label>
              </div>
              <div className="col-sm-10">
                <div className="border-height">
                <ol className="padding-left-25">
                  <li>The entry for the meet is open to all Clubs / Individuals.</li>
                  <li>Swimming events will be held accordance with latest FINA Masters Rules</li>
                  <li>All races will be conducted with ONE START and on Time Trails basis. </li>
                  <li>Decision of the Referee is Final and KSA shall not entertain any protests.</li>
                  <li>Competitors who infringe the FINA Rules are liable for disqualification. </li>
                  <li>Admission Fee of Rs. 400/- per swimmer and Registration Fee of 200/- per swimmer</li>
                  <li>Entry fee of Rs. 100/- per event per swimmer and Rs. 400/- for relay events per team</li>
                  <li>Max. events participation limit for each swimmer is 4</li>
                  <p> For queries, contact Mr. Rohith Babu @ 98444 62077</p>
                </ol>
                </div>
              </div>
              <div className="col-sm-12 margin-tb-20  ">
                <input className="margin-5" type="checkbox" required="true" name="agree_TC" aria-required="true" value={this.state.agree_TC} onChange={this.handleInputChange}/>By clicking here , I agree to the terms & conditions 
              </div>
              {/* Terms and Conditions */}



            <div className="row margin-top-25px">
                  <div className="col-sm-12 text-align-center">
                    <button className="btn btn-size font-size" value="Save">Submit</button>
                  </div>
                </div>
          </form>
          </div>
            {/* Form */}
          </div>}
          {/* Main Form */}
          {/* thankyou Page */}
          {this.state.thankyou == "Yes" && <div> <div className="row whitebg border padding-20">
          <div className="row">
                <div className="col-sm-12 details-padding">
                  <p>Dear <b>{this.state.full_name} ,</b></p>
                  <p>Thank you for registering to <b> {this.state.meet_name}</b></p>
                  <h4> <b>Registration Details: </b></h4>
                  {/* Swimmser name */}
                  <div className="row">
                    <div className="col-sm-2">
                      <label className="control-label label-color margin-top">Swimmer Name : </label>
                    </div>
                    <div className="col-sm-10">
                     <p> {this.state.full_name}</p>
                    </div>
                  </div>
                  {/* Swimmer name */}
                  {/* Group */}
                  <div className="row">
                    <div className="col-sm-2">
                      <label className="control-label label-color margin-top">Group : </label>
                    </div>
                    <div className="col-sm-10">
                     <p> {this.state.swimGroup}</p>
                    </div>
                  </div>
                  {/* Group */}
                  {/* Events Selected */}
                  <div className="row">
                    <div className="col-sm-2">
                      <label className="control-label label-color margin-top">Events Selected : </label>
                    </div>
                    <div className="col-sm-10">
                      <ol className="padding-l-15">
                          {this.displayRegisteredEvents()}
                          
                      </ol>
                    </div>
                  </div>
                  {/* Events Selected */}


                  {/* Amount to be paid */}
                  <div className="row">
                    <div className="col-sm-2">
                      <label className="control-label label-color margin-top">Amount To Be Paid : </label>
                    </div>
                    <div className="col-sm-10">
                     <p> Rs. {this.state.amount_to_pay}</p>
                    </div>
                  </div>
                  <p className="color-red">Your registration will be confirmed only after payment is completed.</p>
                  {/* Amount to be paid */}
                  <div className="row margin-top-25px">
                    <div className="col-sm-12 text-align-center">
                     <button onClick={this.payOnline} className="btn btn-size font-size" value="Save">Pay Online</button>
                    </div>
                  </div>
                </div>
              </div>
           </div>
          </div>}
          {/* Thankyou Page */}
          {/* Payment successful */}
          {this.state.paymentSuccess == "Yes" && <div> <div className="row whitebg border padding-20">
          <div className="col-sm-12 details-padding">
            <p className="margin-b-7">Dear <b>{this.state.full_name}</b> ,</p>
            <p className="margin-b-7">Thank you. </p>
            <p className="margin-b-7">Your payment for the registration of <b>{this.state.meet_name},</b> was successful.</p>
            <p className="margin-b-7">An acknowledgement for the registration has been sent to your email.</p>
            <p className="margin-b-7">We look forward to meet you at the venue.</p>
            <p className="margin-b-7">For any queries, please contact +91 91084 56704 or email us at registrations@swimindia.in</p>
          </div>
          </div>
          </div>}
          {/* Payment successful */}
        </div>
          {/* Skylight for photo Guidelines */}
          <SkyLight dialogStyles={photoGuidelines} hideOnOverlayClicked ref="photoGuidelines">
          <div className="modal-content">
            <div className="modal-header">
              <h4 className="modal-title">Photo Guidelines</h4>
            </div>
            <div className="modal-body">
              <ol className="ol-modal-styling">
                <li>The picture you are uploading should have a minimum 300 dpi, or a minimum of roughly 400 pixels x 400 pixels. A common resolution which is available on most cameras is 640 x 480 pixels.</li>
                <li>If you are taking a photo with your mobile phone, please ensure a quality setting of ‘high’ or ‘medium’. The higher the resolution, the better the outcome is likely to be!</li>
                <li>Please take either a full face view (head-on) or a ¾ view (shoulders slightly turned left or right, but with the head facing the camera)</li>
                <li>Lighting is very important; lighting should be uniform and bright, without casting shadows. Natural light is best, but if you are using artificial lighting be sure it comes from several sources. </li>
                <li>If you do not have natural lighting available, halogen or incandescent light works best. Fluorescent lighting should be avoided.</li>
                <li>Backgrounds are best when they are uniform and neutral: white or off-white is the most common. Solid blue or green backgrounds are often used by professionals, as these can be easily masked out. Make sure the background fills the full frame of the picture behind the person.</li>
                <li>Please ensure you upload photos in .jpg or .png formats only.</li>
              </ol>
            </div>
            <div className="modal-footer">
              <button onClick={this.cancelphotoGuidelines} className="btn btn-size font-size " value="Cancel">Close</button>
            </div>
          </div>
          </SkyLight>
          {/*End  Skylight for photo Guidelines */}

      </div>
    )
  }
}
export default App;
