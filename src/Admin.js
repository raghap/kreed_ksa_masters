import React, { Component } from 'react';
import { render } from 'react-dom'

import Master from './containers/Master'
import App from './containers/App'
import Register from './containers/Register'
import Payment from './containers/Payment'

import Config from   './config/app';

import { Router, Route,hashHistory,IndexRoute } from 'react-router'

class Admin extends Component {

  //Prints the dynamic routes that we need for menu of type fireadmin
  // getFireAdminRoutes(item){
  //   if(item.link=="fireadmin"){
  //     return (<Route path={"/fireadmin/"+item.path} component={Fireadmin}/>)
  //   }else{

  //   }
  // }

  // //Prints the dynamic routes that we need for menu of type fireadmin
  // getFireAdminSubRoutes(item){
  //   if(item.link=="fireadmin"){
  //     return (<Route path={"/fireadmin/"+item.path+"/:sub"} component={Fireadmin}/>)
  //   }else{

  //   }
  // }

  //Prints the Routes
  /*
  {Config.adminConfig.menu.map(this.getFireAdminRoutes)}
  {Config.adminConfig.menu.map(this.getFireAdminSubRoutes)}
  */
  render() {
    return (
      
        <Router history={hashHistory}>
        
          <IndexRoute component={App}></IndexRoute>

          <Route path="/" component={App}/>

          <Route path="/register" component={Register}/>
          
          <Route path="/payment" component={Payment}/>
       
      </Router>
     
    );
  }

}

export default Admin;
